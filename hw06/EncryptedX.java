/////////Michael Weimann
/////HW06 -- develop a program that hides a secret X
//10-22-18

//import scanner
import java.util.Scanner;

//create class
public class EncryptedX {
  
  public static void main (String args[]){
    
    //Initialize scanner
    Scanner scnr = new Scanner(System.in);
 
    //Initialize user input value 
    int num;
    //Prompt input from user for integer between 1 and 100
    System.out.print("Please enter a positive integer between 1 and 100: ");
    while(!scnr.hasNextInt() || (num = scnr.nextInt()) < 1 || num > 100){
      System.out.print("Input invalid, please enter a valid integer between 1 and 100. "); //error response
      System.out.print("Please enter a positive integer between 1 and 100: "); //reprompt for input
      scnr.nextLine();
    }
   scnr.nextLine();
    //Use for loops to x structure
    for (int i = 1; i < num; i++){ // use variables i & j in for loop to assign rows and size 
      System.out.println("");
      for (int j = 1; j < num; j++){
        if (i != j && j != (num - i)){
          System.out.print("*");
        }
         else { 
           System.out.print(" ");
         } 
      }
     
      
    }
  }
}
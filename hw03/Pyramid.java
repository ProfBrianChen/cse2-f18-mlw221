////////Michael Weimann
/////CSE 2 -- Hw03
///Pyramid -- Calculating the volume of a square pyramid
//9-17-18

//import java scanner
import java.util.Scanner;
//create class
public class Pyramid{
  
  public static void main(String args[]){
    
    //tell scanner that we are creating an instance to take input from
    Scanner myScanner = new Scanner( System.in );
    
    //square side
    System.out.print("The square side of the pyramid is (input length): "); //input square side length of pyramid
    double squareSide = myScanner.nextDouble(); //double square side value 
    
    //height
    System.out.print("The height of the pyramid is (input height): "); //input height of pyramid
    double height = myScanner.nextDouble(); //double height of pyramid  
    
    //calculate the volume of the pyramid
    double pyramidVolume = (squareSide * squareSide) * (height/3); // use the formula volume equals the area of the square base times the height divided by three to find the volume
    
    //Print the final volume of the square pyramid
    System.out.println("The volume inside the pyramid is: " + pyramidVolume); 
    
  }
  }
  
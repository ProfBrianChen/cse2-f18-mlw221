/////////Michael Weimann
//////CSE 2 -- Hw03
/////Program #1
///Hurricane -- Calculating the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average and converting it to cubic miles
//9-17-18
import java.util.Scanner;
//create class
public class Convert{
  
 
  public static void main(String args[]){
    
    //tell scanner that we are creating an instance to take input from
    Scanner myScanner = new Scanner( System.in );
    
    //enter affected amount of acres
    System.out.print("Enter the affected area in acres: ");
    double acres = myScanner.nextDouble(); //double acres 
    
    //double the quantity of rain that fell in inches
    System.out.print("Enter the amount of rainfall in the affected area in inches: ");
    double rainfall = myScanner.nextDouble(); //double rainfall
    
    //conversions into cubic miles 
    double cubicFeetRain = (acres) * (43560) * (rainfall/12); //converting to cubic feet by multiplying by total square feet per acre
    double totalVolumeRain = (cubicFeetRain) * (.00000000000679357); // multiply cubic feet by cubic mile to convert to volume 
    
    //print final volume of rainfall in converted units
    System.out.println("The quantity of rain = " + totalVolumeRain + " cubic miles");
    
}
}
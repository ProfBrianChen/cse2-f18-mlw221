////Michael Weimann
///Create program that prompts user for course number, the department name, the number of times the class meets a week, the time of day the course begins, the instructor's name, and the number of students in the course
//lab 05

import java.util.Scanner;

public class Inputs {
  
  public static void main (String args[]) {
    
    //intiialize varibales and junk
    int courseNumber; 
    String depName;
    int timesClassMeets;
    int classTime;
    String instructName;
    int classSize;
    String junk = "";
    
    //declare scanner
    Scanner myScanner = new Scanner(System.in);
  
    
    //Course Number 
    System.out.println("What is the course number?");
    while(!myScanner.hasNextInt()){
      System.out.println("You did not enter an integer, please try again.");
      junk = myScanner.nextLine();
    }
    courseNumber = myScanner.nextInt();
    junk = myScanner.nextLine();
    
    //Department Name
    System.out.println("What is the department name?");
    while(!myScanner.hasNextLine()){
      System.out.println("Please input a string value.");
    }
    depName = myScanner.nextLine();
    
    //Number of Times Class Meets
    System.out.println("How many times does the class meet?");
    while(!myScanner.hasNextInt()){
      System.out.println("You did not enter an integer, please try again.");
      junk = myScanner.nextLine();
    }
    timesClassMeets = myScanner.nextInt();
    junk = myScanner.nextLine();
    
    //Time of day class meets
    System.out.println("What time does the class meet?");
     while(!myScanner.hasNextInt()){
      System.out.println("You did not enter an integer, please try again.");
      junk = myScanner.nextLine();
    }
    classTime = myScanner.nextInt();
    junk = myScanner.nextLine();
    
    //Instructors Name
    System.out.println("What is the instructor's name?");
    while(!myScanner.hasNextLine()){
      System.out.println("Please input a string value.");
    }
    instructName = myScanner.nextLine();
    
    //Number of students in the class
    System.out.println("How many students are in the class?");
    while(!myScanner.hasNextInt()){
      System.out.println("You did not enter an integer, please try again.");
      junk = myScanner.nextLine();
    }
    classSize = myScanner.nextInt();
    junk = myScanner.nextLine();
    
    //Print results
    System.out.println("The course number is: " + courseNumber);
    System.out.println("The department name is: " + depName);
    System.out.println("The amount of times the class meets is: " + timesClassMeets);
    System.out.println("The time the class meets at is: " + classTime);
    System.out.println("The instructor's name is: " + instructName);
    System.out.println("The amount of students in the class is: " + classSize);
  }
}
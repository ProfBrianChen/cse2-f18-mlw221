///////////Michael Weimann
/////////CSE 2
////////HW02
///////9-9-2018
////Arithmetic code that computes cost of shopping items with consideration of sales tax
//
public class Arithmetic{
  
  public static void main(String args[]){
    //initialize input variables
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //assign varibale names to the various total costs
    //pants
    double totalCostPants; //total cost of pants
    double totalSalesTaxPants; //total sales tax charged on pants
    //shirts
    double totalCostShirts; //total cost shirts
    double totalSalesTaxShirts; //total sales tax charged on shirts
    //belt
    double totalCostBelt; //total cost belt
    double totalSalesTaxBelt; //total sales tax charged on belt
    //total costs with and without tax 
    double totalCostBeforeTax; //total cost before tax
    double totalSalesTax; //total sales tax charged on total cost
    //total tansaction cost
    double totalTransCost; 
    
    //calculate values with the initialized variables above
    //pants calculations
    totalSalesTaxPants = (numPants * pantsPrice) * paSalesTax; //total cost of pants 
    totalCostPants = (numPants * pantsPrice); //total cost of pants
    
    //shirt calculations
    totalSalesTaxShirts = (numShirts * shirtPrice) * paSalesTax; //total cost of shirts 
    totalCostShirts = (numShirts * shirtPrice); //total cost of shirts
    
    //belt calculations
    totalSalesTaxBelt = (numBelts * beltCost) * paSalesTax; //total cost of belts 
    totalCostBelt = (numBelts * beltCost); //total cost of belts
    
    //total net costs calculations
    totalCostBeforeTax = totalCostPants + totalCostShirts + totalCostBelt; //net of all items cost before tax
    totalSalesTax = totalSalesTaxPants + totalSalesTaxShirts + totalSalesTaxBelt; //net tax cost on all items 
    totalTransCost = totalCostBeforeTax + totalSalesTax; //total cost of entire transaction with tax included 
    
    //convert to two decimal places
    //convert pants tax
    totalSalesTaxPants *= 100;
    int convPantsTax = (int) totalSalesTaxPants;
    totalSalesTaxPants = (int) convPantsTax / 100.00;
    
    //convert shirt tax
    totalSalesTaxShirts *= 100;
    int convShirtTax = (int) totalSalesTaxShirts;
    totalSalesTaxShirts = (int) convShirtTax / 100.00;
    
    //convert belt tax
    totalSalesTaxBelt *= 100;
    int convBeltTax = (int) totalSalesTaxBelt;
    totalSalesTaxBelt = (int) convBeltTax / 100.00;
    
    //convert tax total on all items
    totalSalesTax *= 100;
    int convTotalTax = (int) totalSalesTax;
    totalSalesTax = (int) convTotalTax / 100.00;
    
    //convert total transaction cost
    totalTransCost *= 100;
    int convTotalTrans = (int) totalTransCost;
    totalTransCost = (int) convTotalTrans / 100.00;
    
    //print statements 
    //print results for pants costs 
    System.out.println("The total cost of pants is $" + totalCostPants);
    System.out.println("The sales tax charged buying pants is $" + totalSalesTaxPants);
    //print results for shirts costs 
    System.out.println("The total cost of shirts is $" + totalCostShirts);
    System.out.println("The sales tax charged buying shirts is $" + totalSalesTaxShirts);
    //print results for belt costs 
    System.out.println("The total cost of belts is $" + totalCostBelt);
    System.out.println("The sales tax charged buying belts is $" + totalSalesTaxBelt);
    //print results for total cost
    System.out.println("The total transaction cost with tax included is $" + totalTransCost);
    System.out.println("The total sales tax of the transaction is $" + totalSalesTax);
   
    
    
    
    
    
    
  }
}
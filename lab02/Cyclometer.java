///////Michael Weimann
////9-5-18
///Lab 02 -- Cyclometer -- Print data about cycling trip
//CSE 2

public class Cyclometer{
  //Main method required for every Java program
  public static void main(String args[]){
    int secsTrip1 = 480; //number of seconds it took to complete trip 1
    int secsTrip2 = 3220; //number of seconds it took to complete trip 2
    int countsTrip1 = 1561; //numnber of counts for trip 1
    int countsTrip2 = 9037; //number of counts for trip 2
    //intermediate variables and output data
    double wheelDiameter = 27.0, //Wheel diameter on bike
    PI = 3.14159, //Record PI as a constant 
    feetPerMile = 5280, //Constant value -- the total feel in one mile
    inchesPerFoot = 12, //Constant value -- the total inches in one foot
    secondsPerMinute = 60; // Constant value -- total seconds in one minutw
    double distanceTrip1, distanceTrip2, totalDistance; //double the distances of the two drips and total distance due to decimal need 
    //Print the numbers stored in the variables that store number of seconds (converted to minutes ) and the counts
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + "counts.");
    //Compute distances and store them
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    // Above gives distance in inches
    // for each count, a rotation of the wheel travels the diameter in inches times PI
    distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI/inchesPerFoot/feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    //Print out the output data.
    System.out.println("Trip 1 was " + distanceTrip1 +" miles");
    System.out.println("Trip 2 was "+ distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance+ " miles");
    

	
 


  } //end of main method
} //end of class


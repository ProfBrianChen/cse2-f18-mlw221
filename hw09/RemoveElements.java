/////////////Michael Weimann
//////CSE 2- Homework 9
////Program 2
///Use three methods called in the main method, randomInput(), delete(list,pos), and remove(list,target) to delete, target, and remove variables from a randomly generated array
//11-27-18
import java.util.*;
public class RemoveElements{
  //main method
  public static void main(String [] arg){
    Scanner scan = new Scanner(System.in); 
    int num[] = new int[10]; //intialize num array
    int newArray1[];
    int newArray2[]; 
    int index,target;
    String answer="";
    do {
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }
    while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[]){
    String out = "{";
    for(int j = 0;j<num.length;j++){
      if(j > 0){
        out += ", ";
      }
  	out += num[j];
    }
	out += "} ";
	return out;
  }
//random input method
public static int [] randomInput(){
  Scanner scnr = new Scanner(System.in); //import scanner
  Random rand = new Random(); //import random number generator
  int [] num = new int[10]; //intilaize num array with array size 10
  for (int i = 0; i < 10; i++){ //loop to size of array
    int temp = (rand.nextInt(10)); //use temp as random number generation variable
    num[i] = temp;
  }
  return num; //return num array
}
//delete method
public static int [] delete(int [] num, int position){
  int[] newNum = new int[9]; //initliaze newNum array to size 9
  int y = 0;
  for (int x = 0; x < newNum.length; x++){ //loop by position 
	  if (position == y) { //if & else staement to delete index number value
      y++;
      x--;
      continue;
	  }
	  else if (position != y) { 
      newNum[x] = num[y];
      y++;
	  }
  }
  return newNum; //returns the new array
}
//method to remove target number
public static int [] remove(int [] num, int target){
  int counter = 0; //intiliaze count
  for(int x = 0; x < num.length; x++){ //for loop
    if (num[x] == target){
      counter++;
    }
  }
  int [] newNum = new int[10 - counter]; //new array for removed target numbner
  int y = 0;
  for(int x = 0; x < num.length; x++){ //for loop equal to length of orginial array
    if(num[x] != target){ 
      newNum[y] = num[x];
      y++;
    }
    if(num[x] == target){ 
      continue;
    }
  }
  return newNum; //return adjusted array
}
} 
  


 

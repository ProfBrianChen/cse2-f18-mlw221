/////////////Michael Weimann
//////CSE 2- Homework 9
////Program 1
///Use binary, lineary, and random scrambling search methods for a user prompted array
//11-26-18

//import scanner
import java.util.*;

//create class
public class CSE2Linear {
  public static void main (String args[]){
    //Initialize scanner
    Scanner scnr = new Scanner(System.in);
    int num;
    int [] userNumbers = new int[15];
    int numCounter = 0;
    int previousInt = 0;
    int searchedInt = 0;
    //Prompt input from user for integer between 1 and 100
    System.out.println("Enter 15 ascending ints for final grades in CSE:");
   //Use while loop with counter
    while(numCounter < 15){
      System.out.println("Please enter a positive integer between 0 and 100: "); //prompt user for 15 integers 
      if(!scnr.hasNextInt()){ //if statement to check if the number entered is an integer
        System.out.println("Input invlaid, you did not enter an integer. Please enter an integer between 0 and 100: ");//error response
        scnr.nextLine(); //eat line
        continue;
      }
      if (scnr.hasNextInt()){
        num = scnr.nextInt();
        if (num < 0 || num > 100){ //make sure number is within range
        System.out.println("Input invalid, the number entered was less than zero. Please enter an integer between 0 and 100: ");
        continue;
        }
        if(num < previousInt){ // make sure number is greater than or equal to previous int
        System.out.println("Input invalid, the number entered is not greater than or equal to the last integer. Please enter an integer between 0 and 100: ");
        continue;           
        }
        if (num >= 0 && num <= 100){
        userNumbers[numCounter] = num;
        previousInt = userNumbers[numCounter];
        numCounter++; //loop
        }
      }
    }
    print(userNumbers); //print array
    System.out.println("Please enter a grade to search for: "); //prompt user to search for grade
    while (searchedInt == 0){
      if (scnr.hasNextInt()){
      searchedInt = scnr.nextInt();
      }
      else {
        System.out.println("Invlid number entered -- not an integer value."); //error statement for search
      }
    }
    binarySearch(userNumbers, searchedInt); //call to binarySearch method
    scramble(userNumbers); //call to scramble method 
    System.out.println("Scrambled: ");
    
    print(userNumbers); //print scrambled numbers
    System.out.print("Enter a grade to search for: "); //search for scrambled grade
    while(!scnr.hasNextInt()){
      System.out.println("Invalid number entered -- not an integer value.");
      scnr.nextInt(); //take next integer
    }
    searchedInt = 0; //initialize grade search variable
    System.out.println("Please enter a grade to search for: ");
    while (searchedInt == 0){
      if (scnr.hasNextInt()){
      searchedInt = scnr.nextInt();
      }
      else {
        System.out.println("Invlid number entered -- not an integer value.");
      }
    }
    linear(userNumbers, searchedInt); //call to linear method
  }
  
  //print method 
  public static void print(int [] grades){
    for(int i = 0; i < grades.length; i ++){
      System.out.print(grades[i] + " ");
    }
    System.out.println(" ");//indent
  }
  //binary search method
  public static void binarySearch (int [] userNumbers, int search){
    int low = userNumbers[0];
    int high = userNumbers[14]; //length of the array minus one
    int mid; 
    boolean numFound = false; 
    int totalLoops = 0; //iteration counter
    while(high >= low){
      mid = ((high)+(low)) / 2; //mid value calculation
 
      if(userNumbers[mid] < search){
        low = mid + 1; //narrow search window
        totalLoops++; //loop by iterating 
      }
      else if(userNumbers[mid] > search){
        high = mid - 1; //narrow search window
        totalLoops++; //loop by iterating
      }
      else{
        System.out.println(search + " was found after " + totalLoops + " iterations."); //print outcome
        totalLoops++;
        return; //return outcome
      }
    }
    if (numFound == false){ //if grade was not found
      System.out.println(search + " was not found after " + totalLoops + " iterations."); //print outcome
    }
  }
  //scramble method
  public static void scramble(int [] userNumbers){
    Random rand = new Random(); //import random generator
    int position = rand.nextInt(userNumbers.length); //random number generation within length and parameters of the array
    for(int i = 0; i < userNumbers.length; i++){
      int hold = userNumbers[i]; //hold variable to copy array
      userNumbers[i] = userNumbers[position];
      userNumbers[position] = hold; //manipulate memory locations
      }
  }
  //linear array
  public static void linear(int [] userNumbers, int find){
    int iterations = 0; //initialize iterations counter
	boolean numFound = false;
    for(int i = 0; i < userNumbers.length; i++){ //loop equal to length of array
      if(userNumbers[i] == find){
        iterations++;
        numFound = true;
        break; //break loop
      }
      else {
    	  iterations++; //keep looping until number is found 
      }
    }
      if (numFound){
        System.out.println(find + " was found after " + iterations + " iterations."); //success outcome print statement 
      }
      else {
    	System.out.println(find + " was not found after " + iterations + " iterations.");  //fail outcome print statement
      }
  }
}

/////Michael Weimann
///CSE2 - Using various methods and a menu to output user data
///10-29-18
//import scanner
import java.util.Scanner;

//create class wordTools
public class wordTools {
  
 static Scanner scnr = new Scanner(System.in);
 //main method
 public static void main(String [] args) {
   //initialize variables within the main method
   System.out.println("Enter a sample text here: "); //usr input sample text
   String sample = "";
   sample = scnr.nextLine();
   sampleText(sample);
   String menuSelect = "";
   String userPhrase = "";
   
   //loop menu options -- repeat and reprompt until user requests to quit
   while (true) {
	   //Menu 
	   System.out.println("MENU");
	   System.out.println("c - Number of non-white space characters");
	   System.out.println("w - Number of words");
	   System.out.println("f - Find text");
	   System.out.println("r - Replace all !'s");
	   System.out.println("s - Shorten spaces");
	   System.out.println("q - Quit");
	   System.out.println("Choose an Option: ");
     menuSelect = scnr.nextLine();
     //if statement to check user input
     if (menuSelect.equals("c") || menuSelect.equals("w") || 
    menuSelect.equals("f") || menuSelect.equals("r") || menuSelect.equals("s") || 
    menuSelect.equals("q")){
       //if statement to check text
       if(menuSelect.equals("f")){
         System.out.println("What phrase/word are you looking for?");
         userPhrase = scnr.nextLine();
       }
       //pass three values into print value method
       printMenu(menuSelect, sample , userPhrase);
     }
       else {
         System.out.println("Please input a valid word/phrase."); //else statement for invalid entry
       }
   }
 }
 //sample text method
 public static void sampleText (String sample) {
   System.out.println("You entered: " + sample);
 }
  //switch statement to organize menu layout
 public static void printMenu (String check, String text, String userPhrase){
     switch(check){
       case "c":
         getNumOfNonWSCharachters(text);
         break;
       case "w":
         getNumOfWords(text);
         break;
       case "f":
         findText(text , userPhrase);
         break;
       case "r":
         System.out.println(replaceExclamation(text));
         break;
       case "s":
         System.out.println(shortenSpace(text));
         break;
       case "q":
         quit();
         break;
     }
   //reprompt user
     System.out.println("Pleace select another option");
     System.out.println("Use the q command to quit the program");
   }

   //Amount of non-whitespace characters
   public static void getNumOfNonWSCharachters(String text) { 
     int wsCounter = 0; // intitiliaze non-whitespace counter
     for (int k = 0; k < text.length(); k++){
       if (text.charAt(k) == ' '){
       }
       else {
         wsCounter++;
       }
     }
     System.out.println("Number of non - whitespace characters =  " + wsCounter); //print non-whitespace total
   }
   //Number of words
 public static void getNumOfWords (String text){
     int wordCounter = 1; // initialize word counter
     for (int k = 0; k < text.length(); k++){
       if (text.charAt(k) == ' '){
         wordCounter++;
       }
       else {
       } 
     }
     System.out.println("Number of words: " + wordCounter); // print total word count
   }
   //Text finder
 public static void findText(String text, String userPhrase){
     int phraseCounter = 0; //initialize phrase counter
     while (text.contains(userPhrase)){ 
       if (text.contains(userPhrase)){
         text = text.replaceFirst(userPhrase, ""); 
         phraseCounter++; 
       }
     }
     System.out.println("Number of phrases: " + phraseCounter); //print numbe rof phrases
   }
  // Exclamation method that replaces each '!' character in the string with a '.' 
 public static String replaceExclamation (String text){
     text = text.replaceAll("!" , ".");
     return text;
   }
  //Space method that returns a string that replaces all sequences of 2 or more spaces with a single space. 
 public static String shortenSpace(String text){
     text = text.replaceAll("( )+" , " ");
     return text;
   }
  //quit method 
 public static void quit () {
     System.exit(0);
   }
}

/////////Michael Weimann
//////CSE2 - HW04
///Create craps simulation that produces random outputs using ONLY switches
//9-24-18

//import scanner
import java.util.Scanner;
//import random die generator
import java.util.Random; 

//create public class 
public class CrapsSwitch {
  
  public static void main(String[] args) {
    //initialize random generator
    Random randGen = new Random();
    //initialize scanner
    Scanner myScanner = new Scanner( System.in );
    
    //initialize string 
    String random = "blank";
    
    //intialize die 1 and die 2
    int die1 = 0;
    int die2 = 0;
    
    //Recieve user input on die preference -- randomly cast dice or if theyd like to state the two dice they want to evaluate
    System.out.println("Would you like randomly cast dice?");
    //input line
    random = myScanner.nextLine(); 
    
    //use switch statements to determine outcome of rolling two dies
    switch(random) {
      case "yes":
        die1 = randGen.nextInt(6) + 1;
        die2 = randGen.nextInt(6) + 1;
        System.out.println("The random die #1 value is: " + die1);
        System.out.println("The random die #2 value is: " + die2); 
        break; 
         
      case "no": 
        System.out.println("Input die #1 value: "); //die #1 input value print statement if no random number generation
        die1 = myScanner.nextInt(); //input value #1
        
        System.out.println("Input die #2 value: ");  ////die #1 input value print statement if no random number generation
        die2 = myScanner.nextInt(); //input value #2
        break; } 
        
      //initialize sum of dies 
      int dieSum = die1 + die2;
        
        switch(dieSum) {
          case 2: 
            System.out.println("Snake Eyes");
            break;
            
          case 3:
            System.out.println("Ace Duece");
            break;
            
          case 4:
            switch(die1) {
              case 2: 
                System.out.println("Hard Four");
                break;
              default: 
                System.out.println("Easy Four");
                break; } 
            break;
            
          case 5: 
            System.out.println("Fever Five");
            break;
            
          case 6:
            switch(die1) {
              case 3:
                System.out.println("Hard Six");
                break;
                default:
                System.out.println("Easy Six");
                break; } 
            break;
            
          case 7:
            System.out.println("Seven Out");
            break;
            
          case 8:
            switch(die1) {
              case 4:
                System.out.println("Hard eight");
                break;
              default:
                System.out.println("Easy Eight");
                break; }
            break;
            
          case 9:
            System.out.println("Nine");
            break;
            
          case 10:
            switch(die1) {
              case 5:
                System.out.println("Hard Five");
                break;
              default:
                System.out.println("Easy Five");
                break; } 
            break;
            
          case 11:
            System.out.println("Yo-leven");
            break;
            
          case 12:
            System.out.println("Boxcars");
            break;
            
                
            }
            
            }
                
    
            
            
           
            }
            
            
       

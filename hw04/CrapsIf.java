/////////Michael Weimann
//////CSE2-HW04
////Create craps simulation that produces random outputs using ONLY if statements
//9-24-18

//import scanner
import java.util.Scanner;
//import random die generator
import java.util.Random; 

//create public class 
public class CrapsIf {
  
  public static void main(String[] args) {
    //initialize random generator
    Random randGen = new Random();
    //initialize scanner
    Scanner myScanner = new Scanner( System.in );
    
    //initialize string 
    String random = "blank";
    
    //intialize die 1 and die 2
    int die1 = 0;
    int die2 = 0;
    
    //Recieve user input on die preference -- randomly cast dice or if theyd like to state the two dice they want to evaluate
    System.out.println("Would you like randomly cast dice?");
    //input line
    random = myScanner.nextLine(); 
    
    //use if statements to either determined the random printed die output or to choose the value of the two die
    //if random 
    if (random.equals("yes")) {
      die1 = randGen.nextInt(6) + 1;
      die2 = randGen.nextInt(6) + 1;
    }
    //if user wishes to input die output numbers
    else if (random.equals("no")) {
      System.out.println("Input die #1 value: "); //die #1 input value print statement if no random number generation
      die1 = myScanner.nextInt(); //input value #1
      
      System.out.println("Input die #2 value: ");  ////die #1 input value print statement if no random number generation
      die2 = myScanner.nextInt(); //input value #2
    }
    //initialize sum of dies
    int dieSum = die1 + die2;
    {
    //both dies are ones
    if (dieSum == 2) {
      System.out.println("Snake Eyes");
    }
    
    //roll a two and a one
    else if (dieSum == 3) {
      System.out.println("Ace Duece");
    }
    
    //roll a one and a three
    else if (dieSum == 4 && die1 != 2) {
      System.out.println("Easy Four");
    }
    
    //roll two twos
    else if (dieSum == 4 && die1 == 2) {
      System.out.println("Hard Four");
    }
    
    //roll a three and a two
    else if (dieSum == 5) {
      System.out.println("Fever Five");
    }
    
    //roll two die that sum to six but none of the dies equals three
    else if (dieSum == 6 && die1 != 3) {
      System.out.println("Easy Six");
    }
    
    //roll two threes
    else if (dieSum == 6 && die1 == 3) {
      System.out.println("Hard Six");
    }
    
    //roll a three and a four
    else if (dieSum == 7) {
      System.out.println("Seven Out");
    }
    //roll two die that sum to 8 but none of the dies equals four
    else if (dieSum == 8 && die1 != 4) {
      System.out.println("Easy Eight");
    }
    
    //roll two fours
    else if (dieSum == 8 && die1 == 4) {
      System.out.println("Hard Eight");
    }
    
    //roll a six and a nine
    else if (dieSum == 9) {
      System.out.println("Nine");
    }
    
    //roll two die that sum to ten but none of the dies equals five 
    else if (dieSum == 10 && die1 != 5) {
      System.out.println("Easy Ten");
    }
    
    //roll two fives
    else if (dieSum == 10 && die1 == 5) {
      System.out.println("Hard Ten");
    }
    
    //roll a five and a six
    else if (dieSum == 11) {
      System.out.println("Yo-leven");
    }
    
    //roll two sixes
    else if (dieSum == 12) {
      System.out.println("Boxcars");
    }
    }
     
    
    }
    }
    
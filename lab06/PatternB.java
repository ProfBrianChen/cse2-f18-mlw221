/////////Michael Weimann
/////Lab 6 -- Pyramid B
//10-17-18

//import scanner
import java.util.Scanner;

//create class
public class PatternB {
  
  public static void main (String args[]){
    
    //Initialize scanner
    Scanner scnr = new Scanner(System.in);
 
    //Initialize varibales
    int numRows;
    
    //Prompt input
    System.out.print("Please enter a positive integer between 1 and 10: ");
    while(!scnr.hasNextInt() || (numRows = scnr.nextInt()) < 1 || numRows > 10){
      System.out.print("Input invalid, please enter a valid integer between 1 and 10. "); //error response
      System.out.print("Please enter a positive integer between 1 and 10: "); //reprompt for input
      scnr.nextLine();
    }
    scnr.nextLine();
    
    //Use for loops to create pyramid structure 
    for (int i = numRows; i >= 1; --i){ //for loop relative to rows
      for (int j = 1; j <= i; ++j){ //nested for loop relative to output
        System.out.print( j + " "); //variable print
      }
      //print new row
      System.out.println();
    }
  }
}
/////////Michael Weimann
//////CSE 2 -- Lab 4
////9-19-18

//import random generator 
import java.util.Random;

//create class
public class CardGenerator {
  
  public static void main(String args[]){
    
    //Initialize and name random number generator
    Random randGen = new Random();

    //initialize card number
    int cardNum = randGen.nextInt(52) + 1;
    // Declare string for the suit of the card
    String cardSuit = "no suit";
    
    //Use if statements to determine the suit of the card
    if (cardNum <= 13){
      cardSuit = "Hearts"; //checking if clubs
    }
    else if (cardNum >= 14 && cardNum <= 26){
      cardSuit = "Diamonds"; //checking if diamonds
      cardNum = cardNum - 13;
    }
    else if (cardNum >= 27 && cardNum <= 39){
      cardSuit = "Clubs"; //checking if clubs
      cardNum = cardNum - 26;
    }
    else if (cardNum >= 40 && cardNum <= 51){
      cardSuit = "Spades"; //checking if spades
      cardNum = cardNum - 39;
    }
    //Print statements use the card number and card suit determined above to print specific card and suit
    if (cardNum < 11 && cardNum  >= 2){
      System.out.println("You picked the " + cardNum + " of " + cardSuit); //Print statement for non-face cardsk and their respective numbers and suits
    }
    else if (cardNum == 1){
      System.out.println("You picked the Ace of " + cardSuit);  //Print statement for Ace and its suit
    }
    else if (cardNum == 11){
      System.out.println("You picked the Jack of " + cardSuit); //Print statement for Jack and its suit
    }
    else if (cardNum == 12){
      System.out.println("You picked the Queen of " + cardSuit); //Print statement for Queen and its suit
    }
    else if (cardNum == 13){
      System.out.println("You picked the King of " + cardSuit); //Print statement for and its suit
    }
    
    }
    }
   
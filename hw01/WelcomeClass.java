///////////Michael Weimann
//////////9-4-18
////////CSE 02 - 210
//////HW01 - Welcome Class 
/////

public class WelcomeClass{
  
  public static void main(String args[]){
    //Print Welcome Class thread to terminal window
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    //Print initials and Lehigh signature
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--L--W--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
                       
    
    
  }
}
////////////Michael Weimann
///////lab 7 -- random number and word generator
////10-24-18

//import scanner
import java.util.Scanner;
import java.util.Random;

//create class
public class Lab07 {
  
  public static void main (String args[]){
    
    //Initialize scanner
    Scanner scnr = new Scanner (System.in);
    boolean reRun = true; 
    //welcome statement before story
    System.out.println("Welcome!");
    //initialize key variables
    do {
      String adj1 = adjectives(); 
      String adj2 = adjectives();
      String noun1 = noun1();
      String noun2 = noun2();
      String verb = verb();
      //Print random sentences and reprompt user
      System.out.println("The " + adj1 + " " + noun1 + " " + verb + " the " + adj2 + " " + noun2 + ".");
      System.out.println("The " + noun1 + " even " + verb() + " the " + noun2 + " for fun.");
      System.out.println("Inesterestingly enough, the " + noun1 + " got very " + adjectives() + " from when it " + verb() + " the "+ noun2 + "earlier in the day.");
      System.out.println("That " + noun1 + " sure had a wild day!");
      System.out.println("Would you like to re-run the program? (y/n)");
      String repeat = scnr.nextLine();
      //Use if statements to check if user wishes to repeat randomly generated story
      if (repeat.equalsIgnoreCase("n")) {
        System.out.println("Goodbye!");
        reRun = false;
      }
      else if (repeat.equalsIgnoreCase("y")) {
        reRun = true;
      }
      else {
        System.out.println("Invlaid user input.  Please run the program again.");
        reRun = false;
      }
      System.out.println();
    }
      while(reRun);
  }
  // list of random adjectives, two sets of nouns, and verbs that random geenrator pulls from
  //noun 1 group of words
  public static String noun1() {
    Random random = new Random ();
    String noun1 = " ";
    int randomInt = random.nextInt(10);
    switch(randomInt){
      case 0:
        noun1 = "dog";
        break;
      case 1:
        noun1 = "cat";
        break;
      case 2:
        noun1 = "tiger";
        break;
      case 3:
        noun1 = "snail";
        break;
      case 4:
        noun1 = "sloth";
        break;
      case 5:
        noun1 = "ant";
        break;
      case 6:
        noun1 = "car";
        break;
      case 7:
        noun1 = "warplane";
        break;
      case 8:
        noun1 = "shoe";
        break;
      case 9:
        noun1 = "phone";
        break;
    }
    return noun1;
  }
  //noun 2 group of words 
  public static String noun2() {
    Random random = new Random ();
    String noun2 = " ";
    int randomInt = random.nextInt(10);
    switch(randomInt){
      case 0:
        noun2 = "dog";
        break;
      case 1:
        noun2 = "cat";
        break;
      case 2:
        noun2 = "tiger";
        break;
      case 3:
        noun2 = "snail";
        break;
      case 4:
        noun2 = "sloth";
        break;
      case 5:
        noun2 = "ant";
        break;
      case 6:
        noun2 = "car";
        break;
      case 7:
        noun2 = "warplane";
        break;
      case 8:
        noun2 = "shoe";
        break;
      case 9:
        noun2 = "phone";
        break;
    }
    return noun2;
  }
  //verb list 
  public static String verb () {
    Random random = new Random (); 
    String verb = " ";
    int randomInt = random.nextInt(10);
    switch(randomInt){
      case 0:
        verb = "fought against";
        break;
      case 1:
        verb = "sped by";
        break;
      case 2:
        verb = "raced with";
        break;
      case 3:
        verb = "outhustled";
        break;
      case 4:
        verb = "jumped over";
        break;
      case 5:
        verb = "outslept";
        break;
      case 6:
        verb = "played with";
        break;
      case 7:
        verb = "swam with";
        break;
      case 8:
        verb = "competed against";
        break;
      case 9:
        verb = "ran through";
        break;
    }
    return verb;
  }
  //adjective list
  public static String adjectives () {
    Random random = new Random ();
    String adjective = " ";
    int randomInt = random.nextInt(10);
    switch (randomInt) {
      case 0:
        adjective = "fast";
        break;
      case 1:
        adjective = "smart";
        break;
      case 2:
        adjective = "intelligent";
        break;
      case 3:
        adjective = "funny";
        break;
      case 4:
        adjective = "large";
        break;
      case 5:
        adjective = "joyful";
        break;
      case 6:
        adjective = "beauitiful";
        break;
      case 7:
        adjective = "tiny";
        break;
      case 8:
        adjective = "tan";
        break;
      case 9:
        adjective = "strong";
        break;   
    }
    return adjective;
  }
}
        
      

    
    
    
    

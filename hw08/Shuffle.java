////////////Michael Weimann
/////Cse2 -- Hw08 
/// User will be given a deck of 52 cards, print out all the cards in the deck, shuffle the whole deck of cards, then print out the cards in the deck, all shuffled, then getting a hand of cards and print them out.
//11-12-18
//import random generator and scanner 
import java.util.Scanner;
import java.util.Random;
public class Shuffle { 
  public static void main(String[] args){ ////main method
    Scanner scnr = new Scanner(System.in); //initialize the scanner 
    //suits club, heart, spade or diamond 
    String[] suitNames = {"C","H","S","D"};    //create array and assign strings
    String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; //new array creation
    String[] hand = new String[5]; 
    int numCards = 5; //intitialixa integer variables
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){    //for look that ensures 52 cards are dealt
      cards[i]=rankNames[i%13]+suitNames[i/13];  //essentially a link between the suitNames and rankNames string that ensures every number asigned has a corresponding suit with it 
    } 
    System.out.println();
    System.out.println("Original Deck: ");
    printArray(cards, index); 
    shuffle(cards, index); 
    printArray(cards, index);
    while(again == 1){ 
      hand = getHand(cards, numCards, index); 
      printArray(hand, numCards);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); //prompt user to see if they want another hand
      again = scnr.nextInt();
    }  
  }
  public static void printArray(String[] cards, int index){ //print array method 
    for (int i = 0; i < index; i++){ //loop with respect to the index to draw 52 cards
      System.out.print(cards[i] + " "); //print corrsponding card and suit values with a space after them
    }
    System.out.println(); //eat liine
  }
  public static void shuffle(String[] cards, int index){  //shuffle method
    Random randomGen = new Random(); //declare random generator name
    System.out.println("Deck Shuffled:");
    for (int i = 0; i < 52; i++){  //loop to print 52 cards
      int mix = randomGen.nextInt(52-i) + i; //create random mix variable 
      String mixCards = cards[mix]; //pass value to cretaed index
      cards[mix] = cards[i]; //swap
      cards[i] = mixCards; // swap
    }
  }
  public static String[] getHand (String[] cards, int numCards, int index){
    String [] hand = new String[5]; //array that stores the value of the five card hand
    System.out.println("Hand:");
    for (int i = 0; i < numCards; i++){
      hand[i] = cards[index]; //store the index array with the shuffled cards array
      index--; //decremenet for next card
    }
    return hand; //return array
  }
}

